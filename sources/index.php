<?php  if(!defined('_source')) die("Error");

    $d->reset(); 
    $sql_banner_top= "select thumb_$lang from #_photo where type='logo_debug'";
    $d->query($sql_banner_top);
    $images_facebook = $d->fetch_array();
    if($images_facebook['thumb_'.$lang]=='')
    {
      $d->reset(); 
      $sql_banner_top= "select thumb_$lang from #_photo where type='logo'";
      $d->query($sql_banner_top);
      $images_facebook = $d->fetch_array();
    }

    $d->reset();
    $sql = "select id,ten_$lang,tenkhongdau,thumb,photo,mota_$lang,new from #_product_list where hienthi=1 and type='dien-thoai' and noibat!=0 order by stt,id";
    $d->query($sql);
    $list_nb = $d->result_array();

    $d->reset();
    $sql = "select id,ten_$lang,tenkhongdau,thumb,photo,mota_$lang,new from #_product_list where hienthi=1 and type='lap-top' and noibat!=0 order by stt,id";
    $d->query($sql);
    $list_nb_lap = $d->result_array();


    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,giaban,thumb from #_product where hienthi=1 and type='san-pham' and noibat!=0 order by stt,id";
    $d->query($sql);
    $product = $d->result_array();

    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,mota_$lang,thumb from #_album where hienthi=1 and type='album' and noibat!=0 order by stt,ngaytao desc";
    $d->query($sql);
    $album = $d->result_array();

    $d->reset();
    $sql= "select * from #_photo where type='anh_qc'";
    $d->query($sql);
    $qc1 = $d->fetch_array();

    $d->reset();
    $sql_gt = "select ten_$lang,mota_$lang,thumb,tenkhongdau from #_info where type ='gioi-thieu'";
    $d->query($sql_gt);
    $row_gt = $d->fetch_array();

    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,thumb,mota_$lang from #_baiviet where hienthi=1 and type='tin-tuc' and noibat!=0 order by stt,ngaysua desc";
    $d->query($sql);
    $tintuc = $d->result_array();

    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,thumb,mota_$lang, photo, giaban, giacu,id from #_product where hienthi=1 and type='lap-top' and banchay!=0 order by stt,ngaysua desc";
    $d->query($sql);
    $spbacc = $d->result_array();

    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,thumb,mota_$lang, baohanh_vi, photo from #_baiviet where hienthi=1 and type='khach-hang' and noibat!=0 order by stt,ngaysua desc";
    $d->query($sql);
    $kh = $d->result_array();

    $d->reset();
    $sql = "select ten_$lang,tenkhongdau,thumb,mota_$lang, baohanh_vi, photo from #_baiviet where hienthi=1 and type='dich-vu' and noibat!=0 order by stt,ngaysua desc";
    $d->query($sql);
    $dichvu = $d->result_array();

    $d->reset();
    $sql_duan = "select ten_vi,thumb_vi,photo_vi,link from #_photo where hienthi=1 and type = 'doitac' order by stt";
    $d->query($sql_duan);
    $doitac = $d->result_array();

    
    $share_facebook = '<meta property="og:url" content="'.getCurrentPageURL_CANO().'" />';
    $share_facebook .= '<meta property="og:type" content="website" />';
    $share_facebook .= '<meta property="og:title" content="'.$row_setting['ten_'.$lang].'" />';
    $share_facebook .= '<meta property="og:description" content="'.$row_setting['description'].'" />';
    $share_facebook .= '<meta property="og:image" content="http://'.$config_url.'/'._upload_hinhanh_l.$images_facebook['thumb_'.$lang].'" />';

    $schema = '<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "https://google.com/article"
      },
      "headline": "'.$row_setting['ten_'.$lang].'",
      "image": {
        "@type": "ImageObject",
        "url": "http://'.$config_url.'/'._upload_hinhanh_l.$logo['thumb_'.$lang].'",
        "height": 800,
        "width": 800
      },
      "datePublished": "'.date('c',$ngaytao_seo).'",
      "dateModified": "2015-02-05T09:20:00+08:00",
      "author": {
        "@type": "Person",
        "name": "'.$row_setting['ten_'.$lang].'"
      },
       "publisher": {
        "@type": "Organization",
        "name": "'.$row_setting['ten_'.$lang].'",
        "logo": {
          "@type": "ImageObject",
          "url": "'.$config_url.'/'._upload_hinhanh_l.$logo['thumb_'.$lang].'",
          "width": 600,
          "height": 60
        }
      },
      "description": "'.strip_tags($row_setting['description']).'"
    }
    </script>
    ';
    
    // if(!empty($_POST)){

    //   if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response_dk'])) {
    //       $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    //       $recaptcha_secret = $config_secretkey;
    //       $recaptcha_response = $_POST['recaptcha_response_dk'];

    //       // Make and decode POST request:
    //       $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
    //       $recaptcha = json_decode($recaptcha);

    //       // Take action based on the score returned:
    //       if ($recaptcha->score >= 0.5) {
    //           // Verified - send email
    //         $hoten=trim(strip_tags($_POST['hotendk']));
    //         $diachi=trim(strip_tags($_POST['diachidk']));
    //         $dienthoai=trim(strip_tags($_POST['dienthoaidk']));
    //         $email=trim(strip_tags($_POST['emaildk']));
    //         $noidung=trim(strip_tags($_POST['noidungdk']));

    //         $hoten = mysqli_real_escape_string($hoten);
    //         $diachi = mysqli_real_escape_string($diachi);
    //         $dienthoai = mysqli_real_escape_string($dienthoai);
    //         $email = mysqli_real_escape_string($email);
    //         $noidung = mysqli_real_escape_string($noidung);

    //         $d->reset();
    //         $sql = "select id from #_newsletter where email='".$email."'";
    //         $d->query($sql);
    //         $maill = $d->result_array();

    //         if(count($maill)!=0){
    //           transfer(_emaildaduocdangky, "index.html");
    //         } else {
    //             $data['email'] =  $email;
    //             $data['ten'] = $hoten;
    //             $data['diachi'] = $diachi;
    //             $data['dienthoai'] = $dienthoai;
    //             $data['noidung'] = $noidung;
    //             $data['ngaytao'] = time();
    //             $d->setTable('newsletter');
    //             if($d->insert($data)){
    //               transfer(_dangkythanhcong, "index.html");
    //             }else{
    //               transfer(_dangkykhongthanhcong, "index.html",false);
    //             }
              
    //         }

    //       }else {transfer(_dangkykhongthanhcong, "index.html",false);}
    //   }else{transfer(_dangkykhongthanhcong, "index.html",false);}

    // }
  
?>