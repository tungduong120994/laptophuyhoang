<?php if(!defined('_source')) die("Error");
		
		$d->reset();
		$sql = "select noidung_$lang,title,keywords,description from #_company where type='lienhe' ";
		$d->query($sql);
		$row_detail = $d->fetch_array();
		
		if(!empty($_POST)){

			if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {

			    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
			    $recaptcha_secret = $config_secretkey;
			    $recaptcha_response = $_POST['recaptcha_response'];

			    // Make and decode POST request:
			    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
			    $recaptcha = json_decode($recaptcha);

			    // Take action based on the score returned:
			    if ($recaptcha->score >= 0.5) {
			        // Verified - send email
			
					$file_name = images_name($_FILES['file']['name']);
					if($file_att = upload_image("file", 'doc|docx|pdf|rar|zip|ppt|pptx|DOC|DOCX|PDF|RAR|ZIP|PPT|PPTX|xls|xlsx|jpg|png|gif|JPG|PNG|GIF', _upload_hinhanh_l,$file_name)){
						$data1['photo'] = $file_att;
						
					}

					include_once "phpMailer/class.phpmailer.php";	
					$mail = new PHPMailer();
					$mail->IsSMTP(); // Gọi đến class xử lý SMTP
					$mail->Host       = $config_host; // tên SMTP server
					$mail->SMTPAuth   = true;                  // Sử dụng đăng nhập vào account
					$mail->Username   = $config_email; // SMTP account username
					$mail->Password   = $config_pass;  

					//Thiet lap thong tin nguoi gui va email nguoi gui
					$mail->SetFrom($config_email,$row_setting['ten_'.$lang]);
					
					$mail->AddAddress($row_setting['email'],$row_setting['ten_'.$lang]);
					
					/*=====================================
					 * THIET LAP NOI DUNG EMAIL
			 		*=====================================*/

					//Thiết lập tiêu đề
					$mail->Subject    = '['.$_POST['ten'].']';
					$mail->IsHTML(true);
					//Thiết lập định dạng font chữ
					$mail->CharSet = "utf-8";	
						$body = '<table>';
						$body .= '
							<tr> 
								<th colspan="2">&nbsp;</th>
							</tr>

							<tr>
								<th colspan="2">Thư liên hệ từ website <a href="http://'.$config_url.'">www.'.$config_url.'</a></th>
							</tr>

							<tr>
								<th colspan="2">&nbsp;</th>
							</tr>

							<tr>
								<th>Họ tên :</th><td>'.$_POST['ten'].'</td>
							</tr>

							<tr>
								<th>Email :</th><td>'.$_POST['email'].'</td>
							</tr>

							<tr>
								<th>Điện thoại :</th><td>'.$_POST['dienthoai'].'</td>
							</tr>
							<tr>
								<th>Tiêu đề :</th><td>'.$_POST['tieude'].'</td>
							</tr>';
							
						$body .= '<tr>
								<th>Nội dung :</th><td>'.$_POST['noidung'].'</td>
							</tr>';
						$body .= '</table>';



						$ten=trim(strip_tags($_POST['ten']));
						$diachi=trim(strip_tags($_POST['diachi']));
						$dienthoai=trim(strip_tags($_POST['dienthoai']));
						$email=trim(strip_tags($_POST['email']));
						$tieude='Thư liên hệ';
						$noidung=trim(strip_tags($_POST['noidung']));

						$ten = mysqli_real_escape_string($d->db, $ten);
						$diachi = mysqli_real_escape_string($d->db, $diachi);
						$dienthoai = mysqli_real_escape_string($d->db, $dienthoai);
						$email = mysqli_real_escape_string($d->db, $email);
						$tieude = mysqli_real_escape_string($d->db, $tieude);
						$noidung = mysqli_real_escape_string($d->db, $noidung);


						$data1['ten'] = $ten;
						$data1['diachi'] = $diachi;
						$data1['dienthoai'] = $dienthoai;
						$data1['email'] = $email;
						$data1['tieude'] = $tieude;
						$data1['noidung'] = $noidung;
						$data1['stt'] = 1;

						$data1['ngaytao'] = time();
						$d->setTable('contact');
						$d->insert($data1);

							
						$mail->Body = $body;

						if($data1['photo']){
							$mail->AddAttachment(_upload_hinhanh_l.$data1['photo']);
						}
				
						
						if($mail->Send())
						{	
							transfer(_guibinhluanthanhcong, "index.html");
						} else {
							transfer(_hethongloi, "index.html",false);
						}

			    } else {
			        transfer(_hethongloi, "index.html",false);
			    }

			}else{transfer(_hethongloi, "index.html",false);}

		}
				
	
?>