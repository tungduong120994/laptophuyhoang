<?php
@define ('_thongbao','Thông báo');
@define ('_hethongloi','Hệ thống lỗi');
@define ('_clickdoi','Click vào đây nếu không muốn đợi lâu');
@define ('_guibinhluanthanhcong','Gửi bình luận thành công');

@define ('_noidungdangcapnhat','Nội dung đang cập nhật');
@define ('_chuanhaptukhoa','Chưa nhập từ khóa');
@define ('_nhaptukhoatimkiem','Nhập từ khóa tìm kiếm');
@define ('_xinnhaphoten','Xin nhập họ tên');
@define ('_xinnhapdiachi','Xin nhập địa chỉ');
@define ('_xinnhapemail','Xin nhập email');
@define ('_nhapdiachiemail','Xin nhập email');
@define ('_checkemail','Email chưa đúng');
@define ('_xinnhapdienthoai','Xin nhập số điện thoại');
@define ('_checkdienthoai','Số điện thoại chưa đúng');
@define ('_xinnhapnoidung','Xin nhập nội dung');
@define ('_xinnhaptieude','Xin nhập tiêu đề');
@define ('_chude','Chủ đề');
@define ('_dangkythanhcong','Đăng ký thành công');
@define ('_dangkykhongthanhcong','Đăng ký không thành công');
@define ('_count_dangky','Bạn đã hết số lần đăng ký. Vui lòng quay lại sau');
@define ('_emaildaduocdangky','Email đã được đăng ký');

// index
@define ('_diachi','Địa chỉ');
@define ('_ngongu','Ngôn ngữ');
@define ('_hotline','Hotline');
@define ('_trangchu','Trang chủ');
@define ('_gioithieu','Giới thiệu');
@define ('_dichvu','Dịch vụ');
@define ('_doitac','Đối tác');
@define ('_tuyendung','Tuyển dụng');
@define ('_daotao','Đào tạo');
@define ('_Sanpham','Sản phẩm');
@define ('_tintuc','Tin tức');
@define ('_lienhe','Liên hệ');
@define ('_timkiem','Tìm kiếm');
@define ('_khongtimthay','không tìm thấy');
@define ('_chungchiiso','Chứng chỉ iso');
@define ('_cungcapdichvu','Cung cấp dịch vụ');
@define ('_cacmuctieudangbaove','Các mục tiêu đang bảo vệ');
@define ('_tatca','Tất cả');
@define ('_tintucsukien','Tin tức sự kiện');
@define ('_ngay','ngày');
@define ('_thang','tháng');
@define ('_nam','năm');
@define ('_xemtatca','Xem tất cả');
@define ('_xemthem','Xem thêm');
@define ('_cameravideo','Camera - Videos');
@define ('_danhgiakhachhang','Đánh giá khách hàng');
@define ('_dangkynhantin','Đăng ký nhận tin');
@define ('_hoten','Họ tên');
@define ('_email','Email');
@define ('_dienthoai','Điện thoại');
@define ('_noidung','Nội dung');
@define ('_gui','Gửi');
@define ('_copyright','2017 Copyright © INTERNATIONAL MR SECURITY. All right reserved. Design by nina.vn');
@define ('_online','Đang online');
@define ('_truycapthang','Truy cập tháng');
@define ('_tongtruycap','Tổng truy cập');


//trang con
@define ('_ngaydang','Ngày đăng');
@define ('_baivietkhac','Bài viết khác');
@define ('_luotxem','Lượt xem');
@define ('_thongtinchitiet','Thông tin chi tiết');
@define ('_binhluanfacebook','Bình luận facebook');
?>