<?php
@define ('_thongbao','Notification');
@define ('_hethongloi','System error');
@define ('_clickdoi','Click here if you do not want to wait');
@define ('_guibinhluanthanhcong','Comment successfully sent');


@define ('_noidungdangcapnhat','Content is updating');
@define ('_chuanhaptukhoa','Keyword not entered');
@define ('_xinnhaphoten','Please enter a first name');
@define ('_xinnhapdiachi','Please enter the address');
@define ('_xinnhapemail','Please enter email');
@define ('_checkemail','Email is not correct');
@define ('_xinnhapdienthoai','Please enter a phone number');
@define ('_checkdienthoai','Phone number is incorrect');
@define ('_xinnhapnoidung','Please enter the content');
@define ('_xinnhaptieude','Please enter a title');
@define ('_chude','Theme');
@define ('_dangkythanhcong','Sign Up Success');
@define ('_dangkykhongthanhcong','Registration failed');
@define ('_count_dangky','You have run out of subscriptions. Please come back later');
@define ('_emaildaduocdangky','The Email was registered');

// index
@define ('_diachi','Address');
@define ('_ngongu','Language');
@define ('_hotline','Hotline');
@define ('_gioithieu','Introduce');
@define ('_dichvu','Service');
@define ('_doitac','Partner');
@define ('_tuyendung','Recruitment');
@define ('_daotao','Educate');
@define ('_tintuc','News');
@define ('_lienhe','Contact');
@define ('_timkiem','Search');
@define ('_khongtimthay','not found');
@define ('_chungchiiso','ISO certificate');
@define ('_cungcapdichvu','Service Provider');
@define ('_cacmuctieudangbaove','Goals are protected');
@define ('_tatca','All');
@define ('_tintucsukien','Event News');
@define ('_ngay','day');
@define ('_thang','month');
@define ('_nam','year');
@define ('_xemtatca','View all');
@define ('_xemthem','See more');
@define ('_cameravideo','Camera - Videos');
@define ('_danhgiakhachhang','Customer Reviews');
@define ('_dangkynhantin','Sign up for');
@define ('_hoten','Name');
@define ('_email','Email');
@define ('_dienthoai','Phone');
@define ('_noidung','Content');
@define ('_gui','Send');
@define ('_copyright','2017 Copyright © INTERNATIONAL MR SECURITY. All right reserved. Design by nina.vn');
@define ('_online','Online');
@define ('_truycapthang','Visit month');
@define ('_tongtruycap','Visit all');


//trang con
@define ('_ngaydang','Date Submitted');
@define ('_tinkhac','Other news');


?>