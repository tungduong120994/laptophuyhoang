<?php	
	error_reporting(E_ALL & ~E_NOTICE & ~8192);
	@define ( '_template' , './templates/');
	@define ( '_source' , './sources/');
    @define ( '_lib' , './libraries/');
	 //Lưu ngôn ngữ chọn vào $_SESSION
    if(!isset($_SESSION['lang']))
    {
        $_SESSION['lang']='vi';
    }
    $lang=$_SESSION['lang'];

	include_once _lib."config.php";
	include_once _lib."constant.php";
	include_once _lib."functions.php";
	include_once _lib."library.php";
	include_once _lib."class.database.php";
	$d = new database($config['database']);
	header("Content-Type: application/xml; charset=utf-8"); 
	echo '<?xml version="1.0" encoding="UTF-8"?>'; 
	echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'; 
	 
	function urlElement($url, $pri,$mod) {
	echo '<url>'; 
	echo '<loc>'.$url.'</loc>'; 
	echo '<changefreq>weekly</changefreq>';
	echo '<lastmod>'.$mod.'</lastmod>';
	echo '<priority>'.$pri.'</priority>';
	echo '</url>';
	}

	urlElement('http://'.$config_url,'1.0',date(c));
	
	$arrcom = array("gioi-thieu","dich-vu","tin-tuc","tuyen-dung","lien-he","tuyen-dung","lap-top","dien-thoai");

	foreach ($arrcom as $k => $v) {
		urlElement('http://'.$config_url.'/'.$v.'.html','1.0',date(c));
	}

	$arrcom_article = array("san-pham","dien-thoai","lap-top");

	for($m = 0, $count_product = count($arrcom_article); $m < $count_product; $m++){
		$d->reset();
		$sql = "select * from table_product_list where type='".$arrcom_article[$m]."'";		
		$d->query($sql);
		$product_list = $d->result_array();
		
		for($k = 0, $count_product_list = count($product_list); $k < $count_product_list; $k++){
			$d->reset();
			$sql = "select * from table_product_cat where type='".$arrcom_article[$m]."' and id_list='".$product_list[$k]['id']."'";		
			$d->query($sql);
			$product_cat = $d->result_array();

				urlElement('http://'.$config_url.'/'.$arrcom_article[$m].'/'.$product_list[$k]['tenkhongdau'],'0.8',date(c,$product_list[$k]['ngaytao']));

						/*for($h = 0, $count_product_cat = count($product_cat); $h < $count_product_cat; $h++){
						urlElement('http://'.$config_url.'/'.$arrcom_article[$m].'/'.$product_list[$k]['tenkhongdau'].'/'.$product_cat[$h]['tenkhongdau'].'.html','0.8',date(c,$product_cat[$h]['ngaytao']));
						}*/
		}

		$d->reset();
		$sql = "select * from table_product where type='".$arrcom_article[$m]."'";		
		$d->query($sql);
		$product = $d->result_array();

		for($pro = 0, $count_product = count($product); $pro < $count_product; $pro++){
			urlElement('http://'.$config_url.'/'.$arrcom_article[$m].'/'.$product[$pro]['tenkhongdau'].'.html','0.8',date(c,$product[$pro]['ngaysua']));
		}
		

	}

	$arrcom_baiviet = array("tin-tuc","dich-vu");
	for($m = 0, $count_baiviet = count($arrcom_baiviet); $m < $count_baiviet; $m++){

		$d->reset();
		$sql = "select * from table_baiviet where type='".$arrcom_baiviet[$m]."'";		
		$d->query($sql);
		$baiviet = $d->result_array();

		for($pro = 0, $count_baiviet = count($baiviet); $pro < $count_baiviet; $pro++){
			urlElement('http://'.$config_url.'/'.$arrcom_baiviet[$m].'/'.$baiviet[$pro]['tenkhongdau'].'.html','0.8',date(c,$baiviet[$pro]['ngaysua']));
		}
	}



	echo '</urlset>'; 

?>
