<?php
	function get_product_name($pid){
		global $d, $row,$lang;
		$sql = "select ten_$lang from #_product where id='".$pid."'";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['ten_'.$lang];
	}
		function get_product_img($pid){

		global $d, $row;

		$sql = "select photo from #_product where id=$pid";

		$d->query($sql);

		$row = $d->fetch_array();

		return $row['photo'];

	}
	function get_product_masp($pid){
		global $d, $row;
		$sql = "select masp from #_product where id='".$pid."'";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['masp'];
	}
	
	function get_price($pid){
		global $d, $row;
		$sql = "select giaban from table_product where id='".$pid."'";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['giaban'];
	}
		
	function get_thumb($pid){
		global $d, $row;
		$sql = "select thumb from table_product where id='".$pid."' ";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['thumb'];
	}
	function remove_product($pid){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
		redirect('gio-hang.html');
	}
	function remove_pro_thanh($pid){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
		redirect('thanh-toan.html');
	}

	function remove_compare($pid){
		$pid=intval($pid);
		$max=count($_SESSION['compare']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['compare'][$i]['productid']){
				unset($_SESSION['compare'][$i]);
				break;
			}
		}
		$_SESSION['compare']=array_values($_SESSION['compare']);
	}
	function get_order_total(){
		$max=count($_SESSION['cart']);
		$sum=0;
		for($i=0;$i<$max;$i++){
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$price=get_price($pid);
			$sum+=$price*$q;
		}
		return $sum;
	}
	function addtocart($pid,$q){
		if($pid<1 or $q<1) return;
		
		if(is_array($_SESSION['cart'])){
			if(product_exists($pid,$q)) return 0;
			$max=count($_SESSION['cart']);
			$_SESSION['cart'][$max]['productid']=$pid;
			$_SESSION['cart'][$max]['qty']=$q;
			return count($_SESSION['cart']);
		}
		else{
			$_SESSION['cart']=array();
			$_SESSION['cart'][0]['productid']=$pid;
			$_SESSION['cart'][0]['qty']=$q;
			return count($_SESSION['cart']);	
		}
	}
	function product_exists($pid,$q){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		$flag=0;
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid']){
				$_SESSION['cart'][$i]['qty'] = $_SESSION['cart'][$i]['qty'] + $q;
				$flag=1;
				break;
			}
		}
		return $flag;
	}
	function get_total(){



		$sum=0;



			if(isset($_SESSION['cart']))

			{

				$max=count($_SESSION['cart']);

		

				for($i=0;$i<$max;$i++){

					$pid=$_SESSION['cart'][$i]['productid'];

					$q=$_SESSION['cart'][$i]['qty'];

					$sum+=$q;

				}

			}

		

		return $sum;

	}

	
	
	function addcompare($pid){
		if($pid<1) return;
		
		if(is_array($_SESSION['compare'])){
			if(compare_exists($pid)) return 0;
			$max=count($_SESSION['compare']);
			$_SESSION['compare'][$max]['productid']=$pid;
			return count($_SESSION['compare']);
		}
		else{
			$_SESSION['compare']=array();
			$_SESSION['compare'][0]['productid']=$pid;
			return count($_SESSION['compare']);	
		}
	}
	function compare_exists($pid){
		$pid=intval($pid);
		$max=count($_SESSION['compare']);
		$flag=0;
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['compare'][$i]['productid']){
				$flag=1;
				break;
			}
		}
		return $flag;
	}

	function get_type($pid){
		global $d, $row;
		$sql = "select type from table_product where id='".$pid."' ";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['type'];
	}

	function get_com($type){
		if($type=="") return "";
		switch($type){
			case 'product':
				$comsp = "tour-trong-nuoc";
				break;
			case 'ngoainuoc':
				$comsp = "tour-ngoai-nuoc";
				break;
			// case 'thietbigiamsat':
			// 	$comsp = "thiet-bi-giam-sat";
			// 	break;
			// case 'thietbikts':
			// 	$comsp = "thiet-bi-ky-thuat-so";
			// 	break;
			// case 'linhkien':
			// 	$comsp = "linh-kien";
			// 	break;
			// case 'phukien':
			// 	$comsp = "phu-kien";
			// 	break;
			// case 'laptop':
			// 	$comsp = "laptop";
			// 	break;
			// case 'mayban':
			// 	$comsp = "may-ban";
			// 	break;
			// case 'thicong':
			// 	$comsp = "thi-cong";
			// 	break;
			// case 'hotroduan':
			// 	$comsp = "ho-tro-du-an";
			// 	break;
			default: 
				$comsp = "san-pham";
				break;
		}
		return $comsp;
	}
	function get_type_bv($pid){
		global $d, $row;
		$sql = "select type from table_baiviet where id='".$pid."' ";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['type'];
	}

	function get_com_bv($type){
      if($type=='nhadatban'){
        return "nha-dat-ban";
      }elseif ($type=='nhadatthue') {
        return "nha-dat-ban-cho-thue";
      }else{
        return "nha-dat-ban";
      }
	}

	function get_tinhthanhlh($pid){
		global $d, $row;
		$sql = "select ten from table_tinh where id='".$pid."' ";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['ten'];
	}

	function get_quanhuyenlh($pid){
		global $d, $row;
		$sql = "select ten from table_quan where id='".$pid."' ";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['ten'];
	}
?>