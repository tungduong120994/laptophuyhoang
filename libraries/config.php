<?php 
	/**
	 * NINA Framework
	 * Vertion 1.0
	 * Author NINA Co.,Ltd. (nina@nina.vn)
	 * Copyright (C) 2015 NINA Co.,Ltd. All rights reserved
	*/

	if(!defined('_lib')) die("Error");
	function nettuts_error_handler($number, $message, $file, $line, $vars)
	{
		if ( ($number !== E_NOTICE) && ($number < 2048) ) {
			include 'templates/error_tpl.php';
			die();
		}
	}
	//set_error_handler('nettuts_error_handler');
	
	error_reporting(0);
 
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	
	$config['salt']='u#9t1cln'; 

	$meta_robots = 'NOODP, INDEX, FOLLOW';  #seo
	$config_url=$_SERVER["SERVER_NAME"].'';
	$config['debug']=1;#Bật chế độ debug dành cho developer
	$config['lang']="vi";
	$config_email="noreply@laptophuyhoang.vn";
	$config_pass="sJgmO1cy";
	$config_host="45.117.170.227";




	$config['database']['servername'] = 'localhost';
	$config['database']['username'] = 'root';
	$config['database']['password'] = '';
	$config['database']['database'] = 'laptophuyh_db';
	$config['database']['refix'] = 'table_';

	$config_recaptcha="6LcN-agUAAAAABFa9l7laEOk-L8fTW9v3f51FM7o";
	$config_secretkey="6LcN-agUAAAAAI2fnckHUm0c3O2jfNUk8Afc4Oi1";

	$config['author']['name'] = '';
	$config['author']['email'] = '';
	$config_mobile ="false";
	$config_mobile_admin ="false";
 
	$fw_conf['firewall']='0'; //Bat tat firewall
	$fw_conf['max_lockcount']=10;//So lan toi da phat hien dau hieu DDOS va khoa IP do vinh vien 
	$fw_conf['max_connect']=15;//So ket noi toi da dc gioi han boi $fw_conf['time_limit']
	$fw_conf['time_limit']=3;//Thoi gian dc thuc hien toi da $fw_conf['max_connect'] ket noi
	$fw_conf['time_wait']=5;//Thoi gian cho de dc mo khoa khi IP bi khoa tam thoi
	$fw_conf['email_admin']='nina@nina.vn';//Email lien lac voi Admin
	$fw_conf['htaccess']=".htaccess";//Duong dan toi file htaccess tren server
	$fw_conf['ip_allow']='127.0.0.1';
	$fw_conf['ip_deny']='';

	$config['login']['delay'] =1;
	$config['login']['attempt']=5;
?>