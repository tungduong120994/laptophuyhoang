<h1 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h1>
<div id="info">

  <div class="wraper_trangtrong">
  
    <h2 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h2>
    <h2 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h2>

    <div class="thanh_title"><h3>Chi tiết sản phẩm</h3></div> 
    <div class="chitiet_sanpham clearfix">

          <div class="imgsp">
              <div class="img_detail">
                  <div class="main_img_detail">
                      <a id="Zoomer" href="<?=_upload_product_l.$row_detail['photo']?>" class="MagicZoomPlus" rel="zoom-width:300px; zoom-height:300px;selectors-effect-speed: 600; selectors-class: Active;">
                          <img class="max_img img_chinh" src="<?=_upload_product_l.$row_detail['photo']?>" alt="<?=$row_detail['tenkhongdau']?>"/>
                      </a>
                  </div><!--end main img detail-->
                  <div id="hinhcon_mo" class="clearfix">
                      <div class="chayhinhcon">                        
                              <div class="item_sub_img">
                                  <a class="Selector img_href0" href="<?=_upload_product_l.$row_detail['photo']?>" rel="zoom-id: Zoomer" rev="<?=_upload_product_l.$row_detail['photo']?>">
                                      <img style="float:left;width:100%" class="transitionAll img_item" src="http://<?=$config_url.'/'._upload_product_l.$row_detail['thumb']?>" alt="<?=$row_detail['ten_'.$lang]?>" title="<?=$row_detail['ten_'.$lang]?>" />
                                  </a>
                              </div><!--end item sub img-->
                              
                          <?php for($i=0, $count = count($product_photos); $i<$count; $i++){ ?>
                              <div class="item_sub_img" >
                                  <a class="Selector img_href<?=$i+1?>" href="<?=_upload_product_l.$product_photos[$i]['photo']?>" rel="zoom-id: Zoomer" rev="<?=_upload_product_l.$product_photos[$i]['photo']?>">
                                      <img class="transitionAll img_item"  src="http://<?=$config_url.'/'._upload_product_l.$product_photos[$i]['thumb']?>" alt="<?=$row_detail['ten_'.$lang]?>" title="<?=$row_detail['ten_'.$lang]?>" />
                                  </a>
                              </div>
                          <?php } ?>
                      </div><!--end sub img detail-->
                  </div>
              </div><!--end img detail-->
          </div>

          <div class="box_des_detail">
              <div class="item_des_detail" style="padding-top:0px;">
                  <span class="ten_detail"><?=$row_detail['ten_'.$lang]?></span>
              </div>
               <?php if($row_detail['giacu']>0) {?>
                <div class="item_des_detail">
                    <span class="giacu_detail">Giá cũ:<?=number_format ($row_detail['giacu'],0,",",".")."vnđ";?></span>
                </div>
              <?php } ?>
              <div class="item_des_detail">
                   <span>Giá: <i><?php if($row_detail['giaban']==0) echo _lienhe; else echo number_format ($row_detail['giaban'],0,",",".")." VNĐ";?></i></span>
              </div>

              <?php if($row_detail['mota_vi']!='') {?>
              <div class="clear"></div>
               <div class="item_des_detail">
                  <?=$row_detail['mota_vi']?>                      
              </div>
              <?php } ?> 
              <div class="item_des_detail bottom_none">
                <span><?=_luotxem?>: <?=$row_detail['luotxem']?></span>                       
              </div>
              <div class="product-action">
                 <div class="product-action-wrap">

                    <input type="hidden" name="" id="product_id" value="<?=$row_detail['id']?> ">
                    <button  class="add-to-cart addcart">MUA NGAY</button>
                    <div class="act-btn">
                       <a class="btn-installment" target="blank" href="https://laptophuyhoang.vn/chinh-sach/ho-tro-tra-gop.html">MUA TRẢ GÓP 0%<span>Thủ tục đơn giản</span></a>
                       <a class="btn-callmeback" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">GỌI LẠI CHO TÔI<span>Sẵn sàng tư vấn</span></a>



                    </div>
                 </div>
              </div>
                                  
              <div class="item_des_detail item_des_social" style="border:none;">
                 <div class="addthis_inline_share_toolbox"></div>
              </div><!--end item des detail-->                   
          </div>
    </div>
    <!-- noidung -->
    <div id="container_product" class="clearfix">
        <div id="tabs" class="clearfix">
            <ul>
                <li class="active"><a href="tab-1"><?=_thongtinchitiet?></a></li>
                <li><a href="tab-2"><?=_binhluanfacebook?></a></li> 
            </ul>
            <div class="content_tabdetail">
                <div id="tab-1" style="display: block;">
                    <div class="noidung_ta" style="clear:left;">
                          <?=$row_detail['noidung_'.$lang]?> 
                    </div>
                </div>
                <div id="tab-2" style="display:none;">
                    <div class="noidung_ta" style="clear:left;background:#fff;">
                        <div class="fb-comments" data-href="<?=getCurrentPageURL_CANO()?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>  
                    </div>       
                </div>
            </div>
        </div>
    </div>
    <!-- sanphamlienquan -->
    <div class="w_splq clearfix">
      <div class="thanh_title"><h3>Sản phẩm liên quan</h3></div> 
      <?php if(count($product)!=0){?>
            <div class="div_css ma-rp">
              <?php foreach($product as $k){?>
                 
                      <div class="item_spptrong">
                        <div class="bor_spppp">
                            <div class="prod_imag">
                                <a href="<?php echo _base_url; ?>/<?=$com?>/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                                  <div class="hid_img">
                                      <img src="<?=_upload_product_l?>/<?=$k['photo']?>" alt="<?=$k['ten_'.$lang]?>" />
                                  </div>
                                </a>
                                
                            </div>
                            <div class="info">
                              <a href="<?php echo _base_url; ?>/<?=$com?>/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                                  <span class="ten_spp"><?=catchuoi($k['ten_'.$lang],60)?></span>
                              </a>
                              <div class="ten_item">
                                <p class="giasi">
                                   <?php if($k['giacu']>0){?>
                                    <span><?=number_format ($k['giacu'],0,",",".")."đ";?></span>
                                  <?php } ?>
                                </p>
                                <p class="giaban">
                                    <span> 
                                      <?php if($k['giaban']==0) echo _lienhe; else echo number_format ($k['giaban'],0,",",".")."đ";?></span>
                                 </p>
                              </div>
                            </div>
                        </div>  
                      </div> 
                
              <?php } ?>
            </div>
      <?php } else { ?>
      <div class="update_content"><?=_noidungdangcapnhat?></div>
      <?php }?>
    </div>

  </div>
</div>


                       <!--modal-->


                  <div id="myModal" class="modal fade " role="dialog" >
                           <div class="baomodal">
                              <div class="wraptopboxlike">
                                 <div class="tit-login">Gọi lại cho tôi</div>
                                 <a data-dismiss="modal" title="Close" class="close">X</a>
                              </div>
                              <div class="content-callback">
                                 <div class="item-pr-call">
                                    <img src="<?=_upload_product_l.$row_detail['photo']?>" class="img-responsive">
                                    <h3><?=$row_detail['ten_vi']?>
                                    </h3>
                                 </div>
                                 <div class="fr-callback">
                                    <form action="goi-cho-toi.html" method="post" id="frmgoichotoi">
                                       <input type="hidden" name="idgct" value="<?=$row_detail['id']?>">
                                       <input type="text" name="namegct" id="namegct" placeholder="Họ tên (bắt buộc)">
                                       <input type="text" name="phonegct" id="phonegct" placeholder="Điện thoại (bắt buộc)">
                                       <input type="text" name="emailgct" id="emailgct" placeholder="Email (bắt buộc)">
                                       <textarea name="contentgct" id="contentgct" placeholder="Nội dung (bắt buộc)"></textarea>
                                       <button id="goichotoigct" type="submit">Gửi</button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>

                       <!-- end modal -->

<h3 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h3>
<h3 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h3>
<h3 class="visit_hidden"><?=$row_detail['ten_'.$lang]?></h3>

