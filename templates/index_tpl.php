<h2 class="visit_hidden"><?=$row_setting['ten_'.$lang]?></h2>
<div id="info" class="clearfix">

<div class="dichvu">
  <div class="margin-auto ">
    <div class="box_dichvu  clearfix">

      <?php foreach ($dichvu as $v) {?>
        <div class="item_dv wow zoomIn">
          <div class="bor_dv">
            <div class="ig_dv">
              <a href="<?php echo _base_url; ?>/khach-hang/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten_'.$lang]?>">
                  <img src="<?=_upload_baiviet_l?>/<?=$v['thumb']?>" alt="<?=$v['ten_'.$lang]?>" />
              </a>
            </div>
            <div class="info_dv">
              <a class="ten_dv text_catchuoi" href="<?php echo _base_url; ?>/dich-vu/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten_'.$lang]?>"><?=$v['ten_'.$lang]?></a>
              <p class="mota_dv text_catchuoi"><?=$v['mota_'.$lang]?></p>
            </div>
          </div>
        </div>
      <?php }?>
    </div>
    
  </div>
</div>

<div class="spbc">
  <div class="margin-auto">
    <div class="thanh_title">
          <h3>Sản Phẩm Bán Chạy</h3>
          <div class="spc2">  
          </div>
          <a class="them_dt" href="<?php echo _base_url; ?>/lap-top.html"><i class="fa fa-undo" aria-hidden="true"></i>Xem Tất Cả SP</a>
        </div>
    <div class=" khungspbanchay">
          <?php foreach($spbacc as $k){?>
              <div class="item_spp  wow zoomIn">
                  <div class="bor_spppp">
                      <div class="prod_imag scale">
                          <a href="<?php echo _base_url; ?>/lap-top/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <div class="hid_img">
                                <img class="mo_1" src="<?=_upload_product_l?>/<?=$k['photo']?>" alt="<?=$k['ten_'.$lang]?>" />
                                <img class="mo_2" src="<?=_upload_product_l?>/<?=getPhotoFrist($k['id'],'lap-top')?>" alt="<?=$k['ten_'.$lang]?>">
                            </div>
                          </a>
                          
                      </div>
                      <div class="info">
                        <a href="<?php echo _base_url; ?>/lap-top/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <span class="ten_spp"><?=catchuoi($k['ten_'.$lang],60)?></span>
                        </a>
                        <div class="ten_item">
                          <p class="giasi">
                             <?php if($k['giacu']>0){?>
                              <span><?=number_format ($k['giacu'],0,",",".")."đ";?></span>
                            <?php } ?>
                          </p>
                          <p class="giaban">
                              <span> 
                                <?php if($k['giaban']==0) echo _lienhe; else echo number_format ($k['giaban'],0,",",".")."đ";?></span>
                           </p>
                        </div>
                      </div>
                  </div>  
                </div>
          <?php } ?>
      </div>
  </div>
</div>

<?php for($i=0;$i<count($list_nb_lap);$i++){
  $d->reset();
  $sql = "select * from #_product_cat where hienthi=1 and id_list='".$list_nb_lap[$i]['id']."' and type='lap-top' order by stt,id limit 0,5";
  $d->query($sql);
  $row_catt_dttt = $d->result_array(); ?>
<?php
    $d->reset();
    $sql = "select * from #_product where hienthi=1 and type='lap-top' and id_list='".$list_nb_lap[$i]['id']."' and noibat!=0 order by stt,id";
    $d->query($sql);
    $lap = $d->result_array();
 ?>
  <div class="content_sanpham" class="clearfix">
      <div class="margin-auto">

        <?php if($list_nb_lap[$i]['new']==1){?>
        <div class="quangcao">
          <div class="item_c111">
            <img class="c11_sp" src="<?php echo _base_url; ?>/<?=_upload_product_l?>/<?=$list_nb_lap[$i]['photo']?>" alt="<?=$list_nb_lap[$i]['ten_'.$lang]?>" />
            <img class="c12_sp" src="<?php echo _base_url; ?>/<?=_upload_product_l?>/<?=$list_nb_lap[$i]['photo']?>" alt="<?=$list_nb_lap[$i]['ten_'.$lang]?>" />
          </div>
        </div>

      <?php } ?>
        <div class="thanh_title">
          <h3><?=$list_nb_lap[$i]['ten_'.$lang]?></h3>
          <div class="spc2">
            <?php foreach ($row_catt_dttt as $hh) {?>
              <div class="item_catssp"><a href="<?php echo _base_url; ?>/lap-top/<?=$list_nb_lap[$i]['tenkhongdau']?>" title="<?=$list_nb_lap[$i]['ten_'.$lang]?>"><?=$hh['ten_'.$lang]?></a></div>
            <?php }?>
          </div>
          <a class="them_dt" href="<?php echo _base_url; ?>/lap-top/<?=$list_nb_lap[$i]['tenkhongdau']?>" title="<?=$list_nb_lap[$i]['ten_'.$lang]?>"><i class="fa fa-undo" aria-hidden="true"></i>Xem Tất Cả SP</a>
        </div>
        <!-- <div class="khungspbanchay div_css1 chay_spsp clearfix"> -->
        <div class="khungspbanchay">
          
            <?php foreach($lap as $k){?>
                <div class="item_spp wow zoomIn">
                  <div class="bor_spppp">
                      <div class="prod_imag scale">
                          <a href="<?php echo _base_url; ?>/lap-top/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <div class="hid_img">
                                <img class="mo_1 "src="<?=_upload_product_l?>/<?=$k['photo']?>" alt="<?=$k['ten_'.$lang]?>" />
                                <img class="mo_2" src="<?=_upload_product_l?>/<?=getPhotoFrist($k['id'],'lap-top')?>" alt="<?=$k['ten_'.$lang]?>">
                            </div>
                          </a>
                          
                      </div>
                      <div class="info">
                        <a href="<?php echo _base_url; ?>/lap-top/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <span class="ten_spp"><?=catchuoi($k['ten_'.$lang],60)?></span>
                        </a>
                        <div class="ten_item">
                          <p class="giasi">
                             <?php if($k['giacu']>0){?>
                              <span><?=number_format ($k['giacu'],0,",",".")."đ";?></span>
                            <?php } ?>
                          </p>
                          <p class="giaban">
                              <span> 
                                <?php if($k['giaban']==0) echo _lienhe; else echo number_format ($k['giaban'],0,",",".")."đ";?></span>
                           </p>
                        </div>
                      </div>
                  </div>  
                </div>
            <?php } ?>
        </div>
      </div>
  </div>
<?php } ?>

<?php for($i=0;$i<count($list_nb);$i++){
    $d->reset();
    $sql = "select * from #_product_cat where hienthi=1 and id_list='".$list_nb[$i]['id']."' and type='dien-thoai' order by stt,id limit 0,5";
    $d->query($sql);
    $row_catt_laptop = $d->result_array();              
?>
<?php
    $d->reset();
    $sql = "select * from #_product where hienthi=1 and type='dien-thoai' and id_list='".$list_nb[$i]['id']."' and noibat!=0 order by stt,id";
    $d->query($sql);
    $sp = $d->result_array();
 ?>
  <div class="content_sanpham" class="clearfix">
      <div class="margin-auto">

         <?php if($list_nb[$i]['new']==1){?>
        <div class="quangcao">
          <div class="item_c111">
            <img class="c11_sp" src="<?=_upload_product_l?>584x200x1/<?=$list_nb[$i]['photo']?>" alt="<?=$list_nb[$i]['ten_'.$lang]?>" />
            <img class="c12_sp" src="<?=_upload_product_l?>584x200x1/<?=$list_nb[$i]['photo']?>" alt="<?=$list_nb[$i]['ten_'.$lang]?>" />
          </div>
        </div>

      <?php } ?>
        <div class="thanh_title">
          <h3><?=$list_nb[$i]['ten_'.$lang]?></h3>
          <div class="spc2">
            <?php foreach ($row_catt_laptop as $hh) {?>
              <div class="item_catssp"><a href="<?php echo _base_url; ?>/dien-thoai/<?=$list_nb[$i]['tenkhongdau']?>" title="<?=$list_nb[$i]['ten_'.$lang]?>"><?=$hh['ten_'.$lang]?></a></div>
            <?php }?>
          </div>
          <a class="them_dt" href="<?php echo _base_url; ?>/dien-thoai/<?=$list_nb[$i]['tenkhongdau']?>" title="<?=$list_nb[$i]['ten_'.$lang]?>"><i class="fa fa-undo" aria-hidden="true"></i>Xem Tất Cả SP</a>
        </div>
        <div class="spc2_mb">
            <?php foreach ($row_catt_laptop as $hh) {?>
              <div class="item_catssp"><a href="<?php echo _base_url; ?>/dien-thoai/<?=$list_nb[$i]['tenkhongdau']?>" title="<?=$list_nb[$i]['ten_'.$lang]?>"><?=$hh['ten_'.$lang]?></a></div>
            <?php }?>
          </div>
          
        

        <div class="khungspbanchay ">
        <!-- <div class="khungspbanchay div_css1 chay_spsp clearfix"> -->
            <?php foreach($sp as $k){?>
                <div class="item_spp wow zoomIn">
                  <div class="bor_spppp">
                      <div class="prod_imag scale">
                          <a href="<?php echo _base_url; ?>/dien-thoai/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <div class="hid_img">
                                <img src="<?=_upload_product_l?>211x178x2/<?=$k['photo']?>" alt="<?=$k['ten_'.$lang]?>" />
                            </div>
                          </a>
                          
                      </div>
                      <div class="info">
                        <a href="<?php echo _base_url; ?>/dien-thoai/<?=$k['tenkhongdau']?>.html" title="<?=$k['ten_'.$lang]?>">
                            <span class="ten_spp"><?=catchuoi($k['ten_'.$lang],60)?></span>
                        </a>
                        <div class="ten_item">
                          <p class="giasi">
                             <?php if($k['giacu']>0){?>
                              <span><?=number_format ($k['giacu'],0,",",".")."đ";?></span>
                            <?php } ?>
                          </p>
                          <p class="giaban">
                              <span> 
                                <?php if($k['giaban']==0) echo _lienhe; else echo number_format ($k['giaban'],0,",",".")."đ";?></span>
                           </p>
                        </div>
                      </div>
                  </div>  
                </div> 
            <?php } ?>
        </div>
      </div>
  </div>
<?php } ?>


<div class="doitac clearfix">
  <div class="margin-auto">
      <div class="chaydoitac">
          <?php if($doitac) {?>
            <?php foreach ($doitac as $k) { ?>
              <div class="item_doitac wow zoomIn">
                   <a href="<?php echo _base_url; ?>/<?=$k['link']?>" target="_bank"  title="<?=$k['ten_'.$lang]?>">
                       <img src="<?=_upload_hinhanh_l?><?=$k['thumb_vi']?>" alt="<?=$k['ten_'.$lang]?>" />
                    </a>
              </div>
            <?php } ?>
          <?php } ?>
      </div>
  </div>
</div>

<div class="gioithieuchung">
  <div class="margin-auto">
    <span class="ten_gt"><?=$row_gt['ten_'.$lang]?></span>
    <div class="mota_gt text_catchuoi"><?=$row_gt['mota_'.$lang]?></div>
  </div>
</div>

<div class="icon_icon">
  <div class="margin-auto ">
    <div class="box_iconn clearfix">
      <div class="item_icon clearfix"><img src="<?php echo _base_url; ?>/images/icon1_03.png"><p><?=$row_setting['diachi_'.$lang]?></p></div>
      <div class="item_icon clearfix"><img src="<?php echo _base_url; ?>/images/icon1_05.png"><p><span>Support 24/7</span><?=$row_setting['dienthoai']?></p></div>
      <div class="item_icon clearfix"><img src="<?php echo _base_url; ?>/images/icon1_07.png"><p><span>Email liên hệ:</span><?=$row_setting['email']?></p></div>
      <div class="item_icon clearfix"><img src="<?php echo _base_url; ?>/images/icon1_09.png"><p><span>Thời gian làm việc</span><?=$row_setting['chunhat']?></p></div>
    </div>
  </div>
</div>
<!-- 
<div class="khachhang clearfix">
  <div class="margin-auto">
    <div class="title_kh">
      <p>Cảm nhận của khách hàng</p>
      <span><?=$row_setting['slogan_vi']?></span>
    </div>
    <div class="box_kh chay_kh clearfix">
      <?php foreach ($kh as $v) {?>
         <div class="item_kh wow zoomIn">
           <div class="img">
              <a href="khach-hang/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten_'.$lang]?>">
                  <img src="<?=_upload_baiviet_l?><?=$v['thumb']?>" alt="<?=$v['ten_'.$lang]?>" />
              </a>
           </div>
           <div class="info_kh">
             <a class="ten_kh" href="khach-hang/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten_'.$lang]?>"><?=$v['ten_'.$lang]?></a>
             <p class="cls_tenkh">Nơi công tác: <?=$v['baohanh_vi']?></p>
             <p class="cls_motakh">"<?=catchuoi($v['mota_'.$lang],135)?>"</p>
           </div>
         </div>
      <?php }?>
    </div>
  </div>
</div> -->


</div>
<h3 class="visit_hidden"><?=$row_setting['ten_'.$lang]?></h3>
<h4 class="visit_hidden"><?=$row_setting['ten_'.$lang]?></h4>
<h5 class="visit_hidden"><?=$row_setting['ten_'.$lang]?></h5>
