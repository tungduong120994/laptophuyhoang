<?php
    $d->reset();
    $sql = "select ten,thumb,url from #_lkweb where hienthi=1 and type='lkweb' order by stt,id";
    $d->query($sql);
    $lienket = $d->result_array();
?>
<div class="top_head">
    <div class="margin-auto clearfix">
        <div class="top_l"><i class="fa fa-map-marker" aria-hidden="true"></i><?=$row_setting['diachi_'.$lang]?></div>
        <div class="top_r">
            <span class="cls_1"><i class="fa fa-envelope" aria-hidden="true"></i><?=$row_setting['email']?></span>
            <span class="cls_2"><i class="fa fa-phone" aria-hidden="true"></i><?=$row_setting['dienthoai']?></span>
            <div class="lienket">
                <?php foreach($lienket as $k){?>
                    <a href="<?=$k['url']?>" target="_blank"><img src="<?=_upload_hinhanh_l?><?=$k['thumb']?>" alt="<?=$k['ten']?>" /></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>