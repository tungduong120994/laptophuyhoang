<?php
    $d->reset();
    $sql = "select noidung_$lang from #_company where type='footer' ";
    $d->query($sql);
    $footer = $d->fetch_array();

    $d->reset();
    $sql_banner_top= "select ten_$lang,tenkhongdau from #_baiviet where type='chinh-sach'";
    $d->query($sql_banner_top);
    $chinhsach = $d->result_array();

    $d->reset();
    $sql_banner_top= "select ten_$lang,tenkhongdau from #_baiviet where type='ho-tro-khach-hang'";
    $d->query($sql_banner_top);
    $chinhsach1 = $d->result_array();

    $d->reset();
    $sql_banner_top= "select thumb_$lang,link from #_photo where type='bocongthuong'";
    $d->query($sql_banner_top);
    $bocongthuong = $d->fetch_array();
 ?>
<div id="bottom_ft" class="clearfix">
	<div class="margin-auto">
		<div class="dangkymail_ft clearfix">
		    <p class="title_nhantin">Đăng ký nhận bản tin</p>
		    <form action="index.html" method="POST" name="frm_nhantin" id="frm_nhantin">
		    	<input name="hotendk" id="hotendk" type="text" class="input hotendk"  placeholder="<?=_hoten?>">
				
				<input name="dienthoaidk" id="dienthoaidk" type="text" class="input dienthoaidk" placeholder="<?=_dienthoai?>">

				<input name="emaildk" id="emaildk" type="text" class="input emaildk" placeholder="<?=_email?>">

				<input type="hidden" name="recaptcha_response_dk" id="recaptchaResponse_dk"> 
				<button type="button" onclick="js_nhantin();">Đăng ký</button>
			</form>
		</div>
		<div class="content_ft clearfix">
			<div class="cot1_ft">
				<div class="ten_cty"><?=$row_setting['ten_'.$lang]?></div>
				<?=$footer['noidung_'.$lang]?>
			</div>
			<div class="cot2_ft">
				<div class="title_ft">chính sách</div>
				<div class="chinhsachh">
					<?php foreach($chinhsach as $k){?>
						<a class="bv_ft" href="chinh-sach/<?=$k['tenkhongdau']?>.html"><?=$k['ten_'.$lang]?></a>
					<?php } ?>
					<?php if($bocongthuong){?>
						<div class="bocongthong">
						    <a href="<?=$bocongthuong['link']?>" title="Bộ công thương"><img src="<?=_upload_hinhanh_l.$bocongthuong['thumb_'.$lang]?>" alt="Bộ công thương"></a>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="cot4_ft">
				<div class="title_ft">Hỗ trợ khách hàng</div>
				<div class="chinhsachh">
					<?php foreach($chinhsach1 as $k){?>
						<a class="bv_ft" href="ho-tro-khach-hang/<?=$k['tenkhongdau']?>.html"><?=$k['ten_'.$lang]?></a>
					<?php } ?>
				</div>
			</div>
			<div class="cot3_ft">
				<div class="title_ft"><p>Fanpage Facebook</p></div>
	            <div class="fb-page" data-href="<?=$row_setting['facebook']?>" data-tabs="timeline" data-width="500" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?=$row_setting['facebook']?>" class="fb-xfbml-parse-ignore"><a href="<?=$row_setting['facebook']?>">Facebook</a></blockquote></div>
			</div>
		</div>
	</div>
</div>
<div id="copyright" class="clearfix">
	<div class="margin-auto">
		<div class="copy_thongke">2019 Copyright © <?=$row_setting['ten_'.$lang]?>. All rights reserved.Design by Vietit</div>
		<div class="thong_ke_bot">
			
			<span>   <!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
						
						</span>
		</div>
	</div>
</div>
<!-- Histats.com  START  (aync)-->
						