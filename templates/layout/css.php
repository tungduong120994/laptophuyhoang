


<link href="<?php echo _base_url; ?>/js/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/style.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/css/default.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/css/fonts.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/css/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="<?php echo _base_url; ?>/text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
<link href="<?php echo _base_url; ?>/css/slick.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/css/slick-theme.css" type="text/css" rel="stylesheet" />
<link href="<?php echo _base_url; ?>/css/animate.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo _base_url; ?>/js/jssor/jssor.css">


<style type="text/css">
		.box_des_detail  .product-action
		{
			margin: 15px 0;
		}
	.box_des_detail  .product-action .product-action-wrap .add-to-cart {
    outline: 0;
    border: none;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    -ms-border-radius: 7px;
    -o-border-radius: 7px;
    border-radius: 7px;
    width: 100%;
    -webkit-box-flex: 1;
    -webkit-flex: 1 0 auto;
    -moz-box-flex: 1;
    -moz-flex: 1 0 auto;
    -ms-flex: 1 0 auto;
    flex: 1 0 auto;
    background: #00adef;
    color: #ffff01;
    text-transform: uppercase;
    font-size: 18px;
    line-height: 60px;
    line-height: 3.33333em;
    text-align: center;
    font-weight: 700;
    margin-bottom: 10px
}

.box_des_detail  .product-action .product-action-wrap .act-btn {
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
    overflow: hidden;
    margin: 0 -4px
}

.box_des_detail  .product-action .product-action-wrap .act-btn a {
    display: block;
    width: 50%;
    margin: 0 4px;
    background: #00b77c;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    -ms-border-radius: 7px;
    -o-border-radius: 7px;
    border-radius: 7px;
    color: #ffff01;
    text-align: center;
    font-size: 16px;
    line-height: 18px;
    line-height: 1.125em;
    font-weight: 700;
    text-decoration: none !important;
    padding: 12px 5px
}/* css gio hang */
.title-thanhtoan {
 
    float: left;
    width: 100%;
    margin-top: 15px;
}.title-thanhtoan h3 {
  font-size: 16px;
    margin-bottom: 10px;
    color: #007db8;
    font-weight: bold;

}


.tit-thanhtoan {
    font-size: 14px;
    font-weight: bold;    width: 100%;
    float: left;
    margin: 10px 0 15px 0;    color: #c08d00;
}.content-one {
    float: left;
}
.content-one .input-icon1{width: 100%!important;}
.content-one .input-icon {
    float: left;
    width: 50%;
    margin-bottom: 10px;
    min-height: 40px;
}

.padding15 {
    padding-left: 15px;
    padding-right: 15px;
    float: left;
}.content-one label {
    margin-top: 2px;
    font-weight: bold;
    font-size: 13px;
    margin-bottom: 10px;
    float: left;
    min-width: 120px;
    margin-right: 10px;
}.content-one label span {
    color: red;
    margin-left: 5px;
    font-weight: bold;
    font-size: 18px;
}
.content-one input {
    box-shadow: none !important;
    border-radius: 0 !important;
    width: calc(100% - 132px);
}
.content-one .input-icon input {
    height: 36px;
    padding-left: 10px;
    border: 1px solid #eee;
}
  
.switch-same-address {
    clear: both;
    padding: 12px 10px;
    background: #eee;
    margin-top: 10px;
    float: left;
    width: 100%;
    font-size: 14px;
    margin-bottom: 10px;
}.switch-same-address p {
    float: right;
    margin: 0;
    margin-left: 10px;
}
 .container2 {
 display: inline-block;
    position: relative;
    padding-left: 30px;
    /* margin-bottom: 12px; */
    cursor: pointer;
    font-size: 13px;
    padding-top: 2px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;    margin-right: 10px;
}

/* Hide the browser's default radio button */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark1 {
  position: absolute;
  top: 0;
  left: 0;
  height: 24px;
  width: 24px;
  background-color: #2196F3;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark1 {
  background-color: #f36f21;
}

/* When the radio button is checked, add a blue background */
.container2 input:checked ~ .checkmark1 {
  background-color: #f36f21;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark1:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container2 input:checked ~ .checkmark1:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container2 .checkmark1:after {
  top: 8px;
  left: 8px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background: white;
}
.content-same-address {
    clear: both;
    float: left;
    width: 100%;
    display: block;
}.k-step-tt {
        padding: 0px 0;
    width: 100%;
    float: left;
    font-weight: bold;
    font-size: 14px;    color: #c08d00;
}

.content-step-tt
{
  width: 100%;
  float: left;
}
ul.list-choose-tt li a {
    display: block;
    text-transform: uppercase;
    font-size: 13px;
    margin-bottom: 5px;
    background: #eee;
    padding: 6px;
    border-radius: 5px;
    margin: 10px 0;
    min-height: 72px;
    border: 1px solid #ddd;
}ul.list-choose-tt li a span {
    color: red;
    font-weight: bold;
    font-size: 20px;
}

ul.list-choose-tt li a:hover, ul.list-choose-tt li.active a {
    background: #007db8;
    color: #fff;
}

ul.list-choose-tt li a i {
    display: block;
    text-transform: none;
    margin-top: 5px;
    font-size: 14px;
}.tab-content-choose {
    clear: both;
    margin: 20px 10px;
    float: left;
    width: 100%;
    line-height: 22px;
}.info-chosse {
    display: none;
}.info-chosse.active {
    display: block;
}

.item-sp-tt {
    float: left;
    width: 100%;
    border-bottom: 1px solid #eee;
    padding-bottom: 10px;
    padding-top: 10px;
    position: relative;
}.item-sp-tt img {
    max-width: 60px;
    float: left;
    margin-right: 15px;
}.item-sp-tt h3 a {
      font-weight: bold;
    font-size: 13px;
    display: block;
    margin-bottom: 6px;
    color: #000;
    line-height: 18px;
}.item-sp-tt span.delete-pr {
    position: absolute;
    right: 0;
    font-size: 17px;
    cursor: pointer;
    top: 32px;
    display: none;
}.item-sp-tt:hover span.delete-pr {
    display: block;
}.item-sp-tt span.delete-pr:hover {
   color:#007db8;
    
}.box-cart-tt {
    clear: both;
    background: #eee;
    margin-top: 10px;
    float: left;
    width: 100%;
    margin-bottom: 10px;
    padding: 15px 15px;
    font-size: 15px;
}
.sum-sp {
    float: left;

    width: 100%;
}
.box-cart-tt span {
    float: right;
    font-weight: bold;
    color: #333;
}.sum-sum-tt {
    margin: 10px 0;
    float: right;
    vertical-align: middle;
    width: 100%;   
    font-size: 14px;
}.sum-sum-tt span {
    float: right;
    font-weight: bold;
    font-size: 23px;
    margin-top: -7px;
    margin-left: 11px;
    color: red;
}
.right-thanhtoan button.sum-dh {
    background: #f36f21;
    width: 100%;
    border: none;
    float: left;
    margin-top: 14px;
    font-size: 18px;
    padding: 12px 0;
    text-transform: uppercase;
    color: #fff;
    border-radius: 5px;
    border-bottom: 3px solid #b75013;
    cursor: pointer;
    outline: none;
}
/* end gio hang */

@media only screen and (max-width: 360px) {
    .box_des_detail  .product-action .product-action-wrap .act-btn a {
        font-size: 14px;
        line-height: 20px;
        line-height: 1.42857em
    }
}

.box_des_detail  .product-action .product-action-wrap .act-btn a span {
    font-weight: 400;
    font-size: 12px;
    display: block
}


.modalDialog {
  position: fixed;
  font-family: Arial, Helvetica, sans-serif;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.22);
  z-index: 99999;
  opacity:0;
  -webkit-transition: opacity 400ms ease-in;
  -moz-transition: opacity 400ms ease-in;
  transition: opacity 400ms ease-in;
  pointer-events: none;
}
.modalDialog:target {
  opacity:1;
  pointer-events: auto;
}
.modalDialog > div {
  width: 535px;
  position: relative;
  border: 1px solid #ccc;
  margin: 10% auto;
  background-color: #fff;
  outline: 0;
}
.close {
  background: #ccc;
  color: #333;
  line-height: 25px;
  position: absolute;
  right: -12px;
  text-align: center;
  top: -9px;
  width: 24px;
  text-decoration: none;
  font-weight: bold;
  -webkit-border-radius: 12px;
  -moz-border-radius: 12px;
  border-radius: 12px;
  -moz-box-shadow: 1px 1px 3px #000;
  -webkit-box-shadow: 1px 1px 3px #000;
  box-shadow: 1px 1px 3px #000;
}
.close:hover {background: #c08d00;color: #fff;}
.baomodal
{
  width: 60%;
    margin: 0 auto;
    margin-top: 5%;
}

.wraptopboxlike {
    background: #006290;
    color: #fff;
    text-transform: uppercase;
    padding: 12px 5px;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
    float: left;
    width: 100%;
    position: relative;
}

.wraptopboxlike a {
    background: none;
    box-shadow: none;
    position: absolute;
    border: none;
    padding: 11px;    margin-right: 20px;
    margin-top: 10px;
    color: #000;
}
.wraptopboxlike a:hover
{
  background: none;
}
.content-callback {
    float: left;
    width: 100%;
    background: #fff;
    padding: 10px;
}.item-pr-call {
    float: left;
    width: 50%;
    text-align: center;
}.fr-callback {
    width: 50%;
    float: left;
    padding: 15px;
}

.fr-callback input, .fr-callback textarea {
    clear: both;
    float: left;
    margin-top: 10px;
    width: 100%;
    padding: 8px;
    border: 1px solid #ddd;
    margin-bottom: 10px;
}.fr-callback button {
    display: inline-block;
    border: none;
    clear: both;
    float: left;
    padding: 10px 29px;
    text-transform: uppercase;
    background: #006290;
    color: #fff;
    cursor: pointer;
}
/**************/


@media only screen and (max-width: 768px){
.item-pr-call,.fr-callback,.baomodal
{
    width: 100%;
}
.item-pr-call h3
{
    font-size: 17px;
    margin-top: 10px;
}
.content-one .input-icon
{
    width: 100%;
}
.switch-same-address > div
{
    width: 100%;margin-top: 10px;
}
.graduate_tabchon 
{
    margin: 0 -15px;
}
}
@media only screen and (min-width: 1220px){


    .graduate_tabchon {
    margin: 0 -10px;    display: flex;
    flex-wrap: wrap;
}ul.list-choose-tt li {
    padding: 0 10px;
}
.left-thanhtoan {
    width: 60%;
}.right-thanhtoan {
    width: 40%;
}

}
</style>
<?/*-----------albumanh---------------*
<link rel="stylesheet" href="js/photobox/photobox/photobox.css">
*/?>