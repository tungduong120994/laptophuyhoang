<?php 
   $d->reset();
   $sql="select * from table_chuyenkhoan where hienthi=1 ";
   $d->query($sql);
   $phuongthuctt=$d->result_array();
   
   ?>


<h1 class="visit_hidden fn"><?=$title_detail?></h1>
<div class="wraper_trangtrong">
   <h2 class="visit_hidden"><?=$title_detail?></h2>
   <h2 class="visit_hidden"><?=$title_detail?></h2>
   <div class="thanh_title">
      <h3><?=$title_detail?></h3>
   </div>
   <div class="content_trangcon clearfix">
   </div>
</div>
<h3 class="visit_hidden"><?=$title_detail?></h3>
<section class="content_trong width_100">
   <div class="block-content width_100">
      <form action="thanh-toan.html" method="POST" id="frmThanhtoan">

        <div class="row">
         <!-- left gio hang -->
         <div class="left-thanhtoan  col-md-7 col-sm-12 col-xs-12">
            <div class="title-thanhtoan">
               <h3>Địa chỉ thanh toán và vận chuyển</h3>
            </div>
            <div class="form-order-cart-thanhtoan">
               <div class="tit-thanhtoan">Thông tin nhận hàng</div>
               <div class="content-one row">
                  <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                     <label>Họ và tên<span>*</span></label>
                     <input type="text" class="form-control" id="names"  name="names" value="">
                  </div>
                  <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                     <label>Điện thoại<span>*</span></label>
                     <input type="text" class="form-control" id="phones"   name="phones" value="">
                  </div>
                   <div class="input-icon input-icon1 col-md-12 col-sm-12 col-xs-12">
                     <label>Email<span>*</span></label>
                     <input type="email" class="form-control" id="email"   name="email" value="">
                  </div>
                  <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                     <label>Ghi chú<span>*</span></label>
                     <input type="text" name="noteds" id="notes"   class="form-control">
                  </div>
                  <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                     <label>Địa chỉ<span>*</span></label>
                     <input type="text" class="form-control"   id="addresss" name="addresss" value="">
                  </div>
               </div>
               <div class="switch-same-address">
                  Thanh toán cùng địa chỉ?
                  <!--        <p><input type="radio" id="co" name="samadd" value="co" >
                     <label for="co"><span class="btn-same-add"></span>Có</label>
                     </p>
                     <p><input type="radio" checked id="khong" value="khong" name="samadd">
                     <label for="khong"><span class="btn-no-same-add active"></span>Không</label>
                     </p>
                     -->
                  <div style="float: right;">
                     <label class="container2">Có
                     <input type="radio" checked="checked" name="choseadd" value="co">
                     <span class="checkmark1"></span>
                     </label>
                     <label class="container2">Không
                     <input type="radio" name="choseadd" value="khong">
                     <span class="checkmark1"></span>
                     </label>
                  </div>
               </div>
               <div class="content-same-address">
                  <div class="tit-thanhtoan">Thông tin thanh toán</div>
                  <div class="content-one row">
                     <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                        <label>Họ và tên</label>
                        <input type="text" class="form-control" id="namer" name="namer">
                     </div>
                     <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                        <label>Điện thoại</label>
                        <input type="text" class="form-control" id="phoner" name="phoner">
                     </div>
                     <div class="input-icon input-icon1 col-md-12 col-sm-12 col-xs-12">
                     <label>Email<span>*</span></label>
                     <input type="email" class="form-control" id="emails"   name="emails" value="">
                  </div>
                     <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                        <label>Ghi chú</label>
                        <input type="text" name="notedr" id="noter" class="form-control">
                     </div>
                     <div class="input-icon col-md-6 col-sm-6 col-xs-12">
                        <label>Địa chỉ</label>
                        <input type="text" class="form-control" id="addressr" name="addressr">
                     </div>
                  </div>
               </div>
               <div class="k-step-tt">Phương thức thanh toán</div>
               <div class="content-step-tt row10">
                    <ul class="list-choose-tt graduate_tabchon ">
                     <?php foreach ($phuongthuctt as $key => $v) {?>
                     <li  data-id="<?=$v['id']?>" class="col-md-6 col-sm-6 col-xs-12 <?=($key==0)?'active':''?>" style="cursor: pointer;">
                        <a  data-toggle="tab"><?=$v['ten_vi']?><span>*</span>
                        <i><?=$v['mota_vi']?></i>
                        </a> 
                     </li>
                     <?php } ?>
                     </ul>
                      <div class="tab-content-choose graduate_tabtext ">
                     <?php foreach ($phuongthuctt as $key => $v) {?>
                     <div class="info-chosse <?=($key==0)?'active':''?>" id="chose_<?=$v['id']?>">
                        <?=$v['noidung_vi']?>
                     </div>
                     <?php } ?>
                     </div>
               </div>
            </div>
         </div>
         <!-- end left gio hang -->
         <!-- right gio hang -->
         <div class="right-thanhtoan col-md-5 col-sm-12 col-xs-12">
            <div class="title-thanhtoan">
               <h3>Sản phẩm đặt mua</h3>
            </div>
            <?php   if(count($_SESSION['cart'])>0){ 

              ?>
            <div class="list-sp-thanhtoan">
               <?php
                  $max=count($_SESSION['cart']);
                  for($i=0;$i<$max;$i++){
                  $pid=$_SESSION['cart'][$i]['productid'];
                  $q=$_SESSION['cart'][$i]['qty'];    
                  
                  $pname=get_product_name($pid);
                
                  
                  $pimg=get_product_img($pid);
                  
                  if($q==0) continue;
                  ?>
               <div class="item-sp-tt">
                  <img class="img-responsive" alt="" src="<?=_upload_product_l.$pimg?>">
                  <h3>
                     <a><?=$pname?> 
                     </a>
                  </h3>
                  <p><?=$q?> x <?=(get_price($pid)!=0)?(number_format(get_price($pid),0, ',', '.')." <sup>đ</sup>"):'Liên hệ' ?></p>
                  <div class="clearfix"></div>
                  <a class="item_sub_sp1 " data-del="<?=$pid?>" >
                  <span class="delete-pr"><i class="fa fa-trash"></i></span>
                  </a>   
               </div>
               <?php } ?>
            </div>
            <div class="box-cart-tt">
               <div class="sum-sp"><?=get_total()?> sản phẩm <span><?=(get_order_total()!=0)?(number_format(get_order_total(),0, ',', '.')." <sup>đ</sup>"):'Liên hệ' ?></span></div>
               <!--<div class="more-pp-tt">Phụ phí thanh toán <span>1.234.000 <sup>đ</sup></span></div>-->
            </div>
            <!--
               <div class="box-mgg">
                  <input type="text" name="company" class="form-control" placeholder="Thẻ quà tặng hoặc mã giảm giá" />
                  <button class="btn-mgg">Áp dụng</button>
               </div>
               -->
            <div class="sum-sum-tt">
               Thành tiền <span><?=(get_order_total()!=0)?(number_format(get_order_total(),0, ',', '.')." <sup>đ</sup>"):'Liên hệ' ?></span>
            </div>
            <input type="hidden" name="httt" id="httt" value="">
            <button type="submit" id="hoantatthanhtoan" class="sum-dh">Đặt hàng</button> &nbsp;&nbsp; <strong id="errormsg"></strong>         
            <?php } else {?>
            <p>Bạn chưa có sản phẩm nào trong giỏ hàng !</p>
            <?php } ?>
         </div>
         <!-- end right gio hang -->
       </div>
      </form>
   </div>
</section>