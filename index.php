<?php
	session_start();
	@define ( '_template' , './templates/');
	@define ( '_source' , './sources/');
	@define ( '_lib' , './libraries/');

	if(!isset($_SESSION['lang']))
	{
	$_SESSION['lang']='vi';
	}
	$lang=$_SESSION['lang'];
  include_once _lib."AntiSQLInjection.php";
	include_once _lib."config.php";
	include_once _lib."constant.php";
	include_once _lib."functions.php";
	include_once _lib."functions_share.php";
	include_once _lib."class.database.php";
	include_once _source."lang_$lang.php";
	include_once _lib."functions_giohang.php";
  include_once _lib."breadcrumb.php";
	include_once _lib."file_requick.php";
	// include_once _lib."counter.php"; 
  // if (file_exists(_lib."nina_firewall.php")){
  //   include_once _lib."nina_firewall.php";
  // }
  // if($config_mobile=='true'){
  //     include_once _lib."Mobile_Detect.php";
  //     $detect = new Mobile_Detect;
  //     $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
  //     if($deviceType != 'computer'){
  //       @define ( '_template' , './m/');

  //     }else{
  //       @define ( '_template' , './templates/');
  //     }
  // }
	
	// if($_GET['lang']!=''){
	// 	$_SESSION['lang']=$_GET['lang'];
	// 	header("location:".$_SESSION['links']);
	// } else {
	// 	$_SESSION['links']=getCurrentPageURL();
	// }
?>
<!DOCTYPE html>
<html lang="vi">
<head>
<!-- <meta name="viewport" content="width=1349" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1" />
<?php include _template."layout/seoweb.php";?>
<?php include _template."layout/css.php";?>
<?=$row_setting['codehead']?>
</head>
<body ondragstart="return false;" ondrop="return false;">
<?php include _template."layout/background.php";?>
<div id="container">
    <?php if($source=='index'){?><h1 class="visit_hidden fn"><?=$row_setting['ten_'.$lang]?></h1><?php } ?>
    <div id="header" class="">
        <?php include _template."layout/header.php";?>
    </div>
    <!---END #header-->
    <div id="menu" class="transi06 clearfix">
        <?php include _template."layout/menu_top.php";?>
    </div>
    <div class="logo_mobile">
      <a href="" title="Trang Chủ"><img src="<?=_upload_hinhanh_l.$logo_top['thumb_'.$lang]?>" alt="Trang chủ"></a>
    </div>
    <!---END #menu-->
    <div id="menu_mobi" class="menu_mobi11" class="clearfix" style="height: auto!important;"> 
        <?php include _template."layout/menu_mb.php";?>
          <div id="giohangtop1" style="    position: absolute;
    width: 35px;
    right: 40px;
    top: 90px;
">
      <a href="gio-hang.html">
        <img style="width: 100%" src="<?php echo _base_url; ?>/images/cart.png">
        <span style="    position: absolute;">(<?=get_total()?>)</span>
      </a>
  </div>
    </div>
    <!---END #menu_mobi-->
    <?php if($source=="index"){?>
        <div id="slide_show" class="clearfix">
             <?php include _template."layout/slider.php";?> 
        </div>
    <?php } ?>
    <!-- breadcrumb -->
    <?php include _template."layout/breadcrumb.php";?> 
    <main id="main" class="clearfix">
        <div id="content">
            <div class="<?php if($source!='index' && $source!='contact') echo 'margin-auto';?>">
                <?php include _template.$template."_tpl.php";?>
            </div>
        </div>
    </main>
    <!---Footer-->
    <footer id="footer">
        <?php include _template."layout/footer.php"; ?>
    </footer>
</div>

<?php if($schema=='') {
    $schema = '<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "https://google.com/article"
      },
      "headline": "'.$row_setting['ten_'.$lang].'",
      "image": {
        "@type": "ImageObject",
        "url": "http://'.$config_url.'/'._upload_hinhanh_l.$logo['thumb_'.$lang].'",
        "height": 800,
        "width": 800
      },
      "datePublished": "'.date('c',$ngaytao_seo).'",
      "dateModified": "2015-02-05T09:20:00+08:00",
      "author": {
        "@type": "Person",
        "name": "'.$row_setting['ten_'.$lang].'"
      },
       "publisher": {
        "@type": "Organization",
        "name": "'.$row_setting['ten_'.$lang].'",
        "logo": {
          "@type": "ImageObject",
          "url": "'.$config_url.'/'._upload_hinhanh_l.$logo['thumb_'.$lang].'",
          "width": 600,
          "height": 60
        }
      },
      "description": "'.strip_tags($row_setting['description']).'"
    }
    </script>
    ';
} ?>
<?php echo $schema;?>
<?=$row_setting['codebody']?>
<?php include _template."layout/js.php";?> 
<?php include _template."layout/face_zalo.php";?>
<?php include _template."layout/call_sms_cd.php";?>
<?php include _template."layout/popup.php";?>
</body>
</html>